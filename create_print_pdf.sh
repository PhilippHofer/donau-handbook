#!/bin/bash

mkdir -p build-print-pdf
pdftk donaulinz-handbook.pdf burst output build-print-pdf/output_%02d.pdf
rm build-print-pdf/doc_data.txt

amount_files=$(ls build-print-pdf/ | wc -l)
while (( amount_files % 4 != 0 )); do
  ((amount_files++))
done

convert xc:none -page a5 build-print-pdf/blank.pdf

filenames=""
for ((i = 1; i <= amount_files/2; i++)); do
    for number in $i $((amount_files - i + 1)); do
        if [[ -f "build-print-pdf/output_$(printf "%02d" $number).pdf" ]]; then
            filenames+="build-print-pdf/output_$(printf "%02d" $number).pdf "
        else
            filenames+="build-print-pdf/blank.pdf "
        fi
    done
done

pdftk $filenames cat output print_.pdf
pdftk print_.pdf cat 1-endsouth output print__.pdf

num_pages=$(pdftk print__.pdf dump_data | awk '/NumberOfPages/{print $2}')
operations=""
for ((i=1; i<=$num_pages; i++)); do
    if (( (i-1) % 4 < 2 )); then
        # This is one of the pages to be left as is
        operations+=" $i"
    else
        # This is one of the pages to be rotated
        operations+=" ${i}north"
    fi
done

pdftk print__.pdf cat $operations output print.pdf

rm print_.pdf
rm print__.pdf
rm -rf build-print-pdf
